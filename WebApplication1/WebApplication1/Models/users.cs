//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public users()
        {
            this.regs = new HashSet<regs>();
        }
    
        public int userID { get; set; }
        public string userName { get; set; }
        public string email { get; set; }
        public Nullable<System.DateTime> regDate { get; set; }
        public Nullable<int> phone { get; set; }
        public Nullable<int> saloonID { get; set; }
        public string password { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<regs> regs { get; set; }
        public virtual saloon saloon { get; set; }
    }
}
