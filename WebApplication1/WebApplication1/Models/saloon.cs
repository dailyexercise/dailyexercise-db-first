//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication1.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class saloon
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public saloon()
        {
            this.regs = new HashSet<regs>();
            this.users = new HashSet<users>();
        }
    
        public int saloonID { get; set; }
        public string saloonName { get; set; }
        public string saloonAdress { get; set; }
        public Nullable<decimal> saloonPhone { get; set; }
        public Nullable<int> saloonContMax { get; set; }
        public Nullable<int> saloonMembers { get; set; }
        public string saloonComments { get; set; }
        public string saloonHourPrice { get; set; }
        public string saloonimage { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<regs> regs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<users> users { get; set; }
    }
}
