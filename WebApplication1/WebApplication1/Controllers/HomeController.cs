﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {

        sporEntities db = new sporEntities();
        WebApplication1.Models.HomeModel dto = new WebApplication1.Models.HomeModel();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Saloon()
        {
            
            dto.Saloons = db.saloon.ToList();
            return View(dto.Saloons);
        }
        

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            return RedirectToAction("Index");
        }

        public ActionResult Registrate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registrate(DateTime date, TimeSpan start, TimeSpan end, String yourself)
        {

            WebApplication1.Models.regs Regs = new WebApplication1.Models.regs();
            Regs.regDate = date;
            Regs.hours = (end - start).Hours;

            db.regs.Add(Regs);
            db.SaveChanges();

            return RedirectToAction("Saloon");
        }
        [HttpPost]
        public ActionResult Login(String fname, String fpass)
        {

            dto.Users = db.users.ToList();

            foreach (var item in dto.Users) {

                if(item.userName.ToLower() == fname.ToLower() && item.password == fpass && !String.IsNullOrEmpty(fname) && !String.IsNullOrEmpty(fpass)) {
                    //login succesfull
                    TempData["username"] = item.userName;
                    if(item.userID == 1)
                        TempData["username"] = "Admin";
                    return RedirectToAction("Index");
                }
            }

            TempData["errorLogin"] = "Wrong username or password";

            return View();
        }

        [HttpPost]
        public ActionResult Register(String username, String pass,String email,String about)
        {

            WebApplication1.Models.users User = new WebApplication1.Models.users();

            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(pass))
            {
                User.userName = username;
                User.password = pass;
                User.email = email;
                User.regDate = DateTime.Now;
                TempData["username"] = username;

                db.users.Add(User);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                TempData["errorReg"] = "Fill the blanks";
                return RedirectToAction("Login");
            }
        }


    }
}